<?php echo doctype('html5'); ?>
<html lang="en-US">
<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow" />
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="content-language" content="en">
<meta name="language" content="english">
<meta name="p:domain_verify" content="4f2b348a630808f0999075712f3331b3"/> 
<meta name="google-site-verification" content="54nYadARsPMM4K-lczFXZq_DT9q9k25ZhR1QUvN8t6c" />


<?php $SEOTitleName = "EventChum";
$SEODescriptionName = "";
if(isset($SEOTitle)) { $SEOTitleName = $SEOTitle; }
if(isset($SEODescription)) { $SEODescriptionName = $SEODescription; } ?>


<meta name="description" content="<?php echo $SEODescriptionName; ?>" />

<!-- Google Authorship and Publisher Markup -->
<link rel="author" href="https://plus.google.com/100914810784521490723/posts"/>
<link rel="publisher" href="https://plus.google.com/100914810784521490723"/>


<script type="text/javascript">
var debugModeVal = 1;
debugMode = function() {
  if(debugModeVal){
    debugger;
  }
  return;
}
</script>

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $SEOTitleName; ?>">
<meta itemprop="description" content="<?php echo $SEODescriptionName; ?>">
<meta itemprop="image" content="http://www.example.com/image.jpg">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@eventchum">
<meta name="twitter:title" content="<?php echo $SEOTitleName; ?>">
<meta name="twitter:description" content="<?php echo $SEODescriptionName; ?>">
<meta name="twitter:creator" content="@eventchum">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="http://www.example.com/image.html">

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $SEOTitleName; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="http://www.example.com/" />
<meta property="og:image" content="http://example.com/image.jpg" />
<meta property="og:description" content="<?php echo $SEODescriptionName; ?>" />
<meta property="og:site_name" content="www.eventchum.com" />
<meta property="article:published_time" content="2013-09-17T05:59:00+01:00" />
<meta property="article:modified_time" content="2013-09-16T19:08:47+01:00" />
<meta property="article:section" content="Article Section" />
<meta property="article:tag" content="Article Tag" />
<meta property="fb:admins" content="Facebook numberic ID" />


<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="author" content="Dixit Chopra"  />
<!-- BEGIN CORE CSS FRAMEWORK -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/css/unsemantic-grid-responsive.css" rel="stylesheet" type="text/css"/>
<!-- END CORE CSS FRAMEWORK -->
<!-- BEGIN CSS TEMPLATE -->
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
 
<!-- END CSS TEMPLATE -->

<!--START REDACTOR SOURCEs-->

 <!--END REDACTor SOURCES-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/jquery-1.8.3.min.js">\x3C/script>')</script>
</head>

<body class="error-body no-top">
  <a href="#top" id="toTop"><i class="fa fa-3x fa-angle-double-up"></i>Top</a>

<div class="page-wrap">

<div role="navigation" class="navbar navbar-default navbar-top navbar-static-top">
<div class="grid-container">
      <div class="compressed">
        <div class="navbar-header">
          <a href="<?php echo base_url();?>" class="navbar-brand compressed"><img alt="eventchum logo" src="<?php echo site_url();?>assets/images/logo.png" width="209" height="52"></a>
        </div>
        <div class="navbar-collapse collapse">
      
    </div><!--/.nav-collapse -->
    </div>

    <ul class="nav nav-tabs">
      <li <?php if($this->uri->segment(1) ==  ARTICLESCONTROLLER ){ echo "class='active'"; }?>><a href="<?php echo site_url(ARTICLESCONTROLLER);?>" >Articles</a></li>
      <li <?php if($this->uri->segment(1) ==  CATEGORIESCONTROLLER ){ echo "class='active'"; }?>><a href="<?php echo site_url(CATEGORIESCONTROLLER);?>" >Categories</a></li>

  
  </ul>
  


  </div>

</div>

    
<?php flush(); ?>
  

<div class="grid-container main-content">
   




