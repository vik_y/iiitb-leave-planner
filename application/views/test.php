<html>
<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body>
<script>
    /*
     To Do
     1. Write a function to get data from the local storage which takes in key as a parameter
     get_local_store_values('key')
     2. When the app runs for the first time, put a json in local storage which has a key, value pair ( 'leave_parent_id'=1)
     3. Synchronization logic - I will upload the picture
     */

    $(document).ready(function(){
        /*--------User side functions start here--------------------------*/

        var get_leave_count = function(val){
            var myobject = {leave_parent_id:1} //Also add session id to the request later
            //Pick up the session and parent id from the local storage and send that along with the post request
            var return_name;
            $.ajax({
                type: 'GET',
                url: 'http://localhost/leave_planner/rest/get_leave_count',
                data: $.param(myobject),
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    console.log(data);
                    $.each(data, function(index,item){
                        alert(data[index].user);
                    })
                })
                .fail(function(){
                    alert("Ajax failed")
                })
        };

        //get_leave_count();

        var get_leave_history = function(val){
            var myobject = {leave_parent_id:1} //Also add session id to the request later
            //Pick up the session and parent id from the local storage and send that along with the post request
            var return_name;
            $.ajax({
                type: 'POST',
                url: 'http://localhost/leave_planner/rest/get_leave_history',
                data: $.param(myobject),
                dataType: 'json',
                encode: true
             })

            .done(function(data){
                console.log(data);
                $.each(data, function(index,item){
                    alert(data[index].user);
                })
            })
            .fail(function(){
                alert("Ajax failed")
            })
        };
        //get_leave_history();

        var apply_for_leave = function(){
            var myobject = {leave_type: 7, from: "11/11/2014", to: "20/11/2014"} //This will come from $(this).serialize()
            var user_info = {leave_parent_id: 1} //Get this info and session from the local storage
            //pick leave_parent_id, pick session_id,
            var return_name;
            $.ajax({
                type: 'POST',
                url: 'http://localhost/leave_planner/rest/apply_for_leave',
                data: $.param(myobject)+'&'+ $.param(user_info),
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    console.log(data);
                    $.each(data, function(index,item){
                        alert(data[index].user);
                    })
                })
                .fail(function(){
                    alert("Ajax failed")
                })
        }

        var cancel_leave_application = function(){
            var myobject = {leave_id: 7} //This will be picked up from the selected row
            var user_info = {leave_parent_id: 1} //Get this info and session from the local storage
            //pick leave_parent_id, pick session_id,
            var return_name;
            $.ajax({
                type: 'POST',
                url: 'http://localhost/leave_planner/rest/cancel_leave_application',
                data: $.param(myobject)+'&'+ $.param(user_info),
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    console.log(data);
                    $.each(data, function(index,item){
                        alert(data[index].user);
                    })
                })
                .fail(function(){
                    alert("Ajax failed")
                })
        }

        //apply_for_leave();



        /*--------------User side functions end here------------*/

        /*---------------Admin side functions start here --------*/
        var get_applications = function(){
            //Returns all pending leave applications
            //backend needs to be done
        }
        var accept_leave = function(){
            var myobject = {leave_id: 3} //This will be picked up from the selected row
            var user_info = {admin_id: 1, admin_session: "key"} //Get this info and session from the local storage
            //Dummy data, needs to be picked up from the database and authentication code needs to b written on the backend
            var return_name;
            $.ajax({
                type: 'POST',
                url: 'http://localhost/leave_planner/rest/accept_leave_admin',
                data: $.param(myobject)+'&'+ $.param(user_info),
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    console.log(data);
                    $.each(data, function(index,item){
                        alert(data[index].user);
                    })
                })
                .fail(function(){
                    alert("Ajax failed")
                })
        }

        var reject_leave = function(){
            var myobject = {leave_id: 4} //This will be picked up from the selected row
            var user_info = {admin_id: 1, admin_session: "key"} //Get this info and session from the local storage
            //Dummy data, needs to be picked up from the database and authentication code needs to b written on the backend
            var return_name;
            $.ajax({
                type: 'POST',
                url: 'http://localhost/leave_planner/rest/reject_leave_admin',
                data: $.param(myobject)+'&'+ $.param(user_info),
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    console.log(data);
                    $.each(data, function(index,item){
                        alert(data[index].user);
                    })
                })
                .fail(function(){
                    alert("Ajax failed")
                })
        }

        accept_leave();
        reject_leave();

        /*--------------Admin side functions end here -----------*/
    })

</script>
</body>
</html>