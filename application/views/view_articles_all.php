<!--Start of Redactor Part-->
<link rel="stylesheet" href="<?php echo base_url()?>assets/redactor/redactor.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/redactor/style.css" />
<script src="<?php echo base_url()?>assets/redactor/redactor.min.js"></script>
<script>
    $(document).ready(function(){
        var get_articles = '<?php echo base_url().'articles/get_articles'?>';
        var add_article = '<?php echo base_url().'articles/add_article'?>';
        var search_url = "<?php echo base_url().'articles/search_articles' ?>";
        var base_url = "<?php echo base_url()?>";

        var loader1_on = function(){
            $('#spinner1').show();
        }

        var loader1_off = function(){
            $('#spinner1').hide();
        }
        var loader2_on = function(){
            $('#spinner2').show();
        }

        var loader2_off = function(){
            $('#spinner2').hide();
        }

        var get_categories = function(){
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url().'categories/get_categories'?>',
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    $.each(data, function(index,item){
                        var name = item.category_name;
                        var id = item.category_id;
                        $('#category_id').append(
                            '<option value=' +id +'>'+name+'</option>'
                        )
                    })
                })

        };


        $('#redactor_content').redactor({
            imageUpload: '/eventchum/articles/upload_image'
        });

        var load_list = function(){
            $.ajax({
                type: 'GET',
                url: get_articles,
                dataType: 'json',
                encode: true
            })
                .done(function(data){
                    $('#article_list').html('');
                    var base = '<?php echo base_url().'articles/view/'?>'
                    console.log(data);
                    var count = 1;
                    $.each(data, function(index, item){
                        var category_id = item.category_id;
                        var category_name = item.category_name;
                        var articles = item.articles;
                        var content = '<div class="grid-50"><div class="grid simple horizontal"><div class="grid-body"><h3>'+category_name+'</h3><a href="articles/categoryview/'+category_id+'"><h5>(View All)</h5></a><ol>'

                        if (articles.length>0){
                            $.each(articles,function(index, object ){
                                console.log(object.article_name)
                                content += '<a href=articles/view/'+object.article_url+'><li>'+object.article_name+'</li></a>'
                                //$('#article_list').append('<li class="list-group-item">' + item.articles.article_id + ' '+item.articles.article_name)
                            })
                            content += '</ol></div></div></div>'
                            $('#article_list').append(content);
                            }
                    })
                })
        }

        load_list();


        get_categories();

        loader1_off();

        var load_articles = function(search, category, page){
            /*
            This function loads articles and different pages also.
            search takes the search string, which you searched for
            search and category are optional parmaeters. Should be used when needed
             */
        };


        /*
        For submission happening but notification not showing, need to fix it later
         */

        $('#article_submit').click(function(event){
            event.preventDefault();
            loader1_on();
            var temp = $(this).parent().parent(); //Needs to be fixed
            var formData = $(this).parent().parent().serialize();
            console.log(formData);
            $.ajax({
                type: 'POST',
                url: add_article,
                data: formData,
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    console.log(data);
                    if(data.status==1){
                        $(temp).find("input[type=text], textarea").val("");
                        $('#redactor').text('');
                        $(temp)[0].reset();
                        loader1_off();
                        $('#myModal').modal('toggle');
                        console.log(data.message);
                        $('#message_ajax_register').html('<div class="alert alert-success">'+data.message+'</div>')
                        load_list();
                    }
                    else{
                        loader1_off();
                        alert(data.message);
                        $('#message_ajax_register').html('<div class="alert alert-danger">'+data.message+'</div>')
                    }
                })
                .fail(function(data){
                    console.log(data);
                    loader1_off();
                })
        });

        var load_result = function(formData){

            $.ajax({
                type: 'GET',
                url: search_url,
                data: formData,
                dataType: 'json',
                encode: true
            })
                .done(function(data){

                    console.log(data);
                    $.each(data.content, function(index, item){
                        var url =  base_url+'articles/view/'+item.article_url;
                        $('#results').append('<div class="grid simple horizontal"><a target="_blank" href='+url+'><li>'+item.article_name+'</li></a></div>');
                    })

                })

        };
        //load_result();
        $('#search_form').validate({
            focusInvalid: true,
            ignore: "",
            rules: {
                string: {
                    minlength: 3,
                    required: true
                }
            },
            invalidHandler: function (event, validator) {
                //display error alert on form submit
            },
            errorPlacement: function (label, element) { // render error placement for each input type
                var icon = $(element).parent('.input-with-icon').children('i');
                var parent = $(element).parent('.input-with-icon');
                icon.removeClass('fa-check').addClass('fa-exclamation');
                parent.removeClass('success-control').addClass('error-control');
                $('<span class="error"></span>').insertAfter(element).append(label)
                var parent = $(element).parent('.input-with-icon');
                parent.removeClass('success-control').addClass('error-control');
            },
            highlight: function (element) { // hightlight error inputs
                var icon = $(element).parent('.input-with-icon').children('i');
                var parent = $(element).parent('.input-with-icon');
                icon.removeClass('fa-check').addClass('fa-exclamation');
                parent.removeClass('success-control').addClass('error-control');

            },
            unhighlight: function (element) { // revert the change done by hightlight

            },
            success: function (label, element) {
                var icon = $(element).parent('.input-with-icon').children('i');
                var parent = $(element).parent('.input-with-icon');
                icon.removeClass("fa-exclamation").addClass('fa-check');
                parent.removeClass('error-control').addClass('success-control');
            },
            submitHandler: function(form) {
                $('#results').html('')
                event.preventDefault();
                $form = $(form);
                $('html,body').animate({scrollTop: $form.css('top')}, 800);
                var url = $form.attr('action');
                var dataString = $form.serialize();
                $.ajax({
                    type: 'GET',
                    url: search_url,
                    data: dataString,
                    dataType: 'json',
                    encode: true
                })
                    .done(function(data){
                        $('#article_list').html('');
                        console.log(data);
                        $.each(data.content, function(index, item){
                            var url =  base_url+'articles/view/'+item.article_url;
                            $('#results').append('<div class="grid simple horizontal"><a target="_blank" href='+url+'><li>'+item.article_name+'</li></a></div>');
                        })

                    })
            }
        });
        /*$('#search_form').on('submit', function(event){
            event.preventDefault();
            $('#article_list').html('');
            var formData = $(this).serialize();
            load_result(formData);
        })*/


    });


</script>
<!--End of Redactor Part-->

<div class="grid-100 mobile-grid-100">
    <div class="grid simple horizontal">

        <div class="grid-body">
        <!-- Button trigger modal -->
            <div class="grid-title row"><?php echo $breadcrumb; ?>

                <div class="clearfix"></div>
            </div>

        <!-- Modal -->

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" area-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add a New Article</h4>
                    </div>

                    <div class="form-group">
                        <form id="add_article">
                            <div class="modal-body">

                                <div class="center-text" id="spinner1"><i class="fa fa-spinner fa-5x fa-spin"></i></div>

                                <!--WYSIWYG Editor Starts here -->
                                <div class="control-group">
                                    <label class="control-label" for="article_name">Article Name</label>
                                    <div class="controls">
                                        <input id="article_name" name="article_name" type="text" placeholder="Article Name here" required="">

                                    </div>
                                </div>

                                <!-- Select Basic -->
                                <div class="control-group">
                                    <label class="control-label" for="selectbasic">Select Basic</label>
                                    <div class="controls">
                                        <select id="category_id" name="category_id" class="input-xlarge">
                                            <!--Options here will come from jquery-->
                                        </select>
                                    </div>
                                </div>
                                <br>

                                    <textarea id="redactor_content" name="article_content" placeholder="Write and Upload content here">
                                        <p>Enter Article content here</p>
                                    </textarea>

                                <!--WYSIWYG Editor Ends here -->
                            </div>
                            <div class="modal-footer">
                                <button  type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button id="article_submit" type="button" class="btn btn-primary">Save changes</button>

                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>


        <div class="row">
            <div class="jumbotron">
                <div class="row row-fluid">

                    <div class="grid-70">
                        <div id="message_ajax_register"> </div>
                        <h1>Eventchum Help Articles</h1>
                        <button class="btn btn-primary btn-lg btn-danger" data-toggle="modal" data-target="#myModal">
                            Add a new article
                       </button>
                    </div>

                    <div class="grid-30">
                        <form id="search_form" class="navbar-form navbar-left" role="search" action="<?php echo base_url().'articles/search'?>" method="get">
                            <div class="form-group">
                                <input type="text" name="string" class="form-control" placeholder="Search">
                            </div>
                            <input type="submit" id="search_button" class="btn btn-default" value="Submit">
                        </form>
                    </div>

                </div>
            </div>
        </div>



            <br>
            <div class="grid-100">
                <!--Article List starts here-->
                <ul id="article_list" class="list-group">

                </ul>
                <!--Article List ends here-->

                <!--Search List starts here-->
                <ul class="list-unstyled" id="results">

                </ul>
                <!--Search list ends here-->
            </div>
        </div>
    </div>
</div>
