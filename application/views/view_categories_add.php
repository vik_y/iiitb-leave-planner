<script type="text/javascript">
    $(document).ready(function() {

        var load_table_url = '<?php echo base_url().'categories/get_categories'?>';
        var add_category_url = '<?php echo base_url().'categories/add_category'?>';
        var edit_category_url = '<?php echo base_url().'categories/edit_category'?>';
        var delete_category_url = '<?php echo base_url().'categories/delete_category'?>';
        /*
        Function definitions start here
         */
        var loader1_on = function(){
            $('#spinner1').show();
        }

        var loader1_off = function(){
            $('#spinner1').hide();
        }
        var loader2_on = function(){
            $('#spinner2').show();
        }

        var loader2_off = function(){
            $('#spinner2').hide();
        }

        var load_table = function(){
            $.ajax({
                type: 'GET',
                url: load_table_url,
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    $('#categories_table').html('');
                    $.each(data, function(index, item){
                        $('#categories_table').append(
                            /*
                            The Edit and Delete Buttons are not appearing in a row since added the form tag,
                            Can you fix it? There might be some class error orsomething there.
                            */

                            '<tr>'+
                                '<td><p class="hlabel">'+item.category_name+'</p>' +
                                '<form id="edit_category">'+
                                '<input class="txtcategoryname" name="txtcategoryname" type="text" style="display: none" value="'+item.category_name+'"></input>'+
                                '<input type="hidden" name="category_id" value='+item.category_id+'>'+
                                '<input type="submit" class="update" style="display: none" value="Update">'+
                                '<a class="update_hide btn-link" style="display:none">Cancel</a>'+
                                '</form>'+
                                '</td>'+
                                '<td><a id="edit_submit" class="btn btn-small btn-success">Edit &nbsp &nbsp &nbsp</a><form id="delete_category_form">  '+
                                '<input type="hidden" name="category_name" value="'+item.category_name+'"><input type="hidden" name="category_id" value='+item.category_id+'><input type="submit" class="btn btn-small btn-danger" value="Delete"></form></div></td>'
                        );
                    })
                })
                .fail(function(){
                    $('#categories_table').html('<div class="alert alert-danger">No network available</div>')
                })

        };

        /*
        Need to write a function for uploding photos
        /*
        Function definitions end here
        */

        /* loader1 is on the left and loader 2 on the right, they are initialized*/
        loader1_off();
        loader2_off();
        load_table(); //Loads the table of category names



        /*
        ADD Category Starts Here
         */
        $('#add_category-form').submit(function(event){
            $('#message_ajax_register_left').html('');
            $('#message_ajax_register_right').html('');
            event.preventDefault(); //Preventing the default submit action
            loader2_on(); //Switching on the right loader
            $('#message_ajax_register_left').html(''); //Clear the left notifier
            $("#createcategory").attr('disabled',true);
            $("#createcategory").addClass('disabled');
            //get the form data
            var formData = $(this).serialize();

            console.log(formData);
            //process the form
            $.ajax({
                type    : 'POST',
                url     : add_category_url,
                data    : formData,
                dataType : 'json',
                encode  : true
            })

                .done(function(data){
                    console.log(data);
                    if (data.status ==1){
                        loader2_off();
                        $('#message_ajax_register').html('<div class="alert alert-error">'+data.message+'</div>');
                        load_table();
                        $("#createcategory").attr('disabled',false);
                        $("#createcategory").removeClass('disabled');
                    }
                    else{
                        loader2_off();
                        $('#message_ajax_register').html('<div class="alert alert-error">'+data.message+'</div>');
                        $("#createcategory").attr('disabled',false);
                        $("#createcategory").removeClass('disabled');
                    }
                })

                .fail(function(){
                    loader2_off();
                    $("#createcategory").attr('disabled',false);
                    $("#createcategory").removeClass('disabled');
                })

        });
        /*
        ADD Category ends here
         */


        /*
        The code below handles the action after cliking the Edit Button
         */
        $(document.body).on('click','#edit_submit', function(){
           $('.txtcategoryname, .update, .update_hide').hide();
           $('.hlabel').show();
           $(this).parent().prev().children('form').children('.txtcategoryname, .update, .update_hide').show();
           $(this).parent().prev().children('form').children().find('.hlabel').attr('style','display:none');
           $(this).parent().prev().children('p').hide();
        });


        /*
        The code below handles the action after pressing the cancel button
         */
        $(document.body).on('click', '.update_hide', function(){
            $(this).hide();
            $(this).prev().hide();
            $(this).prev().prev().prev().hide();
            $(this).parent().parent().children('p').show();
        });


        /*
        EDIT Category main code starts here
        Code below handles the Category Name upation
         */
        $(document.body).on('submit','#edit_category',function(event){
            $('#message_ajax_register_left').html('');
            $('#message_ajax_register_right').html('');
            loader1_on();
            event.preventDefault();
            $('#message_ajax_register_left').html('');
            var formData = $(this).serialize();
            console.log(formData);
            $.ajax({
                type    : 'POST',
                url     : edit_category_url,
                data    : formData,
                dataType : 'json',
                encode  :true
            })
                .done(function(data){
                    console.log(data);
                    if(data.status==1){
                        $('#message_ajax_register_left').html('<div class="alert alert-success">'+data.message+'</div>') ;
                        load_table();
                        loader1_off();
                    }
                    else{
                        loader1_off();
                        $('#message_ajax_register_left').html('<div class="alert alert-danger">'+data.message+'</div>');
                    }
                })
                .fail(function(data){
                    loader1_off();
                    $('#message_ajax_register_left').html('<div class="alert alert-danger">Request Failed</div>');
                });
        });
        /*
        EDIT Category ends here
         */


        /*
        DELETE category starts here
         */


        //Delete needs to be fixed, form feature is added recently
        $(document.body).on('submit', '#delete_category_form', function(event){
            event.preventDefault();

            $('#message_ajax_register_left').html('');
            $('#message_ajax_register_right').html('');
            var formData = $(this).serialize();
            console.log(formData);

            if(confirm("Are you sure you want to delete "+$(this).children('input').val()+"?")){
                loader1_on();

                $.ajax({
                    type: 'POST',
                    url     : delete_category_url,
                    data    : formData,
                    dataType : 'json',
                    encode  :true
                })
                    .done(function(data){
                        console.log(data);
                        if(data.status==1){
                            loader1_off();
                            $('#message_ajax_register_left').html('<div class="alert alert-success">Deleted Successfully</div>');
                            load_table();
                            $(temp).parent().parent().hide();
                        }
                        else{

                            loader1_off();
                            $('#message_ajax_register_left').html('<div class="alert alert-danger">'+data.message+'</div>');
                        }

                    })
                    .fail(function(data){
                        loader1_off();
                        $('#message_ajax_register_left').html('<div class="alert alert-danger">Request Failed</div>');
                    })
            }
        });
    });


</script>


<!-- Add Category Screen-->
<div class="grid-100 mobile-grid-100">
    <div class="grid simple horizontal">

        <div class="grid-title row"><?php echo $breadcrumb; ?>
            <div class="grid-50 mobile-grid-100"><h3 class="semi-bold text-error row">Add/Edit  Category.</h3></div>
            <div class="clearfix"></div>
        </div>

        <div class="grid-body">
            <div class="row">
                <div class="row">
                    <div class="grid-60 mobile-grid-100">
                        <hr>
                        <h2>Existing Categories</h2>
                        <hr>

                        <div id='message_ajax_register_left'></div>

                        <div class="center-text" id="spinner1"><i class="fa fa-spinner fa-5x fa-spin"></i></div>
                        <table  id="categories_table" class="table table-bordered">

                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Edit/Delete</th>
                            </tr>
                            </thead>
                            <tbody id="categories_table">

                            </tbody>
                        </table>
                    </div>



                <hr style="width:0.5px; height:500px; position: absolute; left: 315px;"/>


                    <div class="grid-40 mobile-grid-100">
                        <?php
                        $attributes = array('id'=>'add_category-form');
                        /*Form Opened
                          for adding category
                         */
                        echo form_open('categories/add_category',$attributes);?>
                        <hr>
                        <h2>Add a New Category</h2>
                        <hr>
                        <div class="center-text" id="spinner2"><i class="fa fa-spinner fa-5x fa-spin"></i></div>
                        <div class="form-group">
                            <?php echo "<div id='message_ajax_register'></div>";?>
                            <div class="row">
                                <div class="pull-left width-30-form">
                                    <?php $data = array('class'=>'form-label');
                                    echo form_label('Category Name<span class="text-error">*</span>','username',$data);?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="pull-left width-100-form no-margin-top">
                                    <div class="controls">
                                        <div class="input-with-icon  right">
                                            <i class="fa"></i>
                                            <?php $data = array('id'=>'txtcategoryname','name'=>'txtcategoryname','class'=>'form-control');
                                            echo form_input($data); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div>
                                <?php $data = array('id'=>'createcategory', 'name'=>'createcategory','value'=>'Create Category','class'=>'btn btn-primary btn-cons has-spinner');
                                echo form_submit($data);

                                echo anchor(base_url().'categories', 'Back', array('class'=>'btn btn-white btn-cons','id'=>'backurl'));
                                ?>
                            </div>
                        </div>
                        <?php
                        echo form_close();
                        /*Form Closed*/
                        ?>
                    </div>
                </div>


                <hr class='flag'/>

            </div>
        </div>
    </div>
</div>


