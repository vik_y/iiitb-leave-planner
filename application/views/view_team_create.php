<script type="text/javascript">
$(document).ready(function() {	
var getTeamCategoriesURL= "<?php echo site_url(get_team_type_categories); ?>";
var getDeleteCategoryURL = "<?php echo site_url(get_delete_category_details); ?>";
var getEditCategoryURL = "<?php echo site_url(get_edit_category_details); ?>";
var categoryJSON='';
var fest_id = <?php echo $fest_id; ?>;
getAllCategories();
$('#txtmembercategory option:first-child').attr("selected", "selected");
//Date Pickers
	var currentFestURL = "<?php echo site_url().'fest/fest-details/'.$fest_id.'/'.$fest_name; ?>";	
	$('#backurl').attr('href',currentFestURL);
	
	var categoryArray = [];	



	function getAllCategories(){
		var categoriesURL = getTeamCategoriesURL+'/'+fest_id;
		$.getJSON(categoriesURL,function(result){
      categoryJSON = result;
      var options = $("#txtmembercategory");
			options.html('');
      var categorytable = $("#categorytable");
      categorytable.html('');
      var categoryTableValues = "<table class='table table-bordered no-more-tables'><thead><tr><th width='60%'>Name</th><th>Edit / Delete</th></tr></thead><tbody>";
			$(result).each(function(j,item){
        $(item).each(function(i,details){
          options.append($("<option />").val(details.member_category_id).text(details.member_category_name));
          var actionForm = '<form action="'+getDeleteCategoryURL+'" method="post" accept-charset="utf-8" class="delete-category-form pull-left"><input type="hidden" name="member_category_id" value="'+details.member_category_id+'" id="member_category_id" /><input type="hidden" name="festId" value="'+fest_id+'" id="festId" /><a class="btn btn-link text-error underline padding-0 margin-right-10" id="editCategoryValue">Edit</a><input type="submit" name="delete" value="delete" class="btn btn-link text-error underline padding-0"></form>';
          var editForm = '<form action="'+getEditCategoryURL+'" method="post" accept-charset="utf-8" class="edit-category-form pull-left" style="display:none;"><input type="hidden" name="member_category_id" value="'+details.member_category_id+'" id="member_category_id" /><input type="hidden" name="festId" value="'+fest_id+'" id="festId" /><input type="text" name="txtmemberCategoryName" value="'+details.member_category_name+'" id="txtmemberCategoryName" /><input type="submit" name="delete" value="Save" class="btn btn-primary"></form>';
          var categoryNameValue = "<span class='categoryName'>"+details.member_category_name+"</span>";
          categoryTableValues += "<tr><td>"+editForm+" "+categoryNameValue+"</td><td>"+actionForm+"</td></tr>";
			   });
      });
      categoryTableValues += "</tbody></table>";
      categorytable.html(categoryTableValues);
		});  
	}

  $("body").on("click","#addEditCategories", function(e){ 
    e.preventDefault();
    $('#addEditCategoriesModal').modal('show');
  });

  $("body").on("click","#editCategoryValue", function(e){ 
    e.preventDefault();
    debugMode();
    $(this).parents().find('.edit-category-form').hide();
    $(this).parents().find('.categoryName').show();
    $(this).parent().parent().parent().find('.edit-category-form').show();
    $(this).parent().parent().parent().find('.categoryName').hide();
  });

  


  $('#register-form').validate({
    focusInvalid: false, 
    ignore: "",
    rules: {
      txtmembercategory: {
        required: true,
      },
      txtmembername: {
        minlength: 2,
        required: true
      },
      txtmemberemail: {
        minlength: 2,
        required: true,
        email:true
      },
      txtmemberphone: {
        required: true,
        minlength: 10,
        digits: true,
        maxlength: 10
      },
      txtlinkedinwebsite: {
        url: true
      },
      txtfacebookwebsite: {
        url: true
      },
      txttwitterwebsite: {
        url: true
      },
      txtgpluswebsite: {
        url: true
      }
    },
    invalidHandler: function (event, validator) {
					//display error alert on form submit    
        },
                errorPlacement: function (label, element) { // render error placement for each input type   
                 var icon = $(element).parent('.input-with-icon').children('i');
                 var parent = $(element).parent('.input-with-icon');
                 icon.removeClass('fa-check').addClass('fa-exclamation');  
                 parent.removeClass('success-control').addClass('error-control');   
                 $('<span class="error"></span>').insertAfter(element).append(label)
                 var parent = $(element).parent('.input-with-icon');
                 parent.removeClass('success-control').addClass('error-control');
               },
                highlight: function (element) { // hightlight error inputs
                 var icon = $(element).parent('.input-with-icon').children('i');
                 var parent = $(element).parent('.input-with-icon');
                 icon.removeClass('fa-check').addClass('fa-exclamation');  
                 parent.removeClass('success-control').addClass('error-control');   

               },
                unhighlight: function (element) { // revert the change done by hightlight

                },
                success: function (label, element) {
                 var icon = $(element).parent('.input-with-icon').children('i');
                 var parent = $(element).parent('.input-with-icon');
                 icon.removeClass("fa-exclamation").addClass('fa-check');
                 parent.removeClass('error-control').addClass('success-control');  
               },
               submitHandler: function(form) {
                $form = $(form);
                $form.ajaxSubmit({ 
                  dataType:  'json',
                  success: processJSONResponse
                });
                return false;
              }
            });


$('#add-category-form').validate({
    focusInvalid: false, 
    ignore: "",
    rules: {
      txtcategoryname: {
        minlength: 2,
        required: true
      }
    },
    invalidHandler: function (event, validator) {
          //display error alert on form submit    
        },
                errorPlacement: function (label, element) { // render error placement for each input type   
                 var icon = $(element).parent('.input-with-icon').children('i');
                 var parent = $(element).parent('.input-with-icon');
                 icon.removeClass('fa-check').addClass('fa-exclamation');  
                 parent.removeClass('success-control').addClass('error-control');   
                 $('<span class="error"></span>').insertAfter(element).append(label)
                 var parent = $(element).parent('.input-with-icon');
                 parent.removeClass('success-control').addClass('error-control');
               },
                highlight: function (element) { // hightlight error inputs
                 var icon = $(element).parent('.input-with-icon').children('i');
                 var parent = $(element).parent('.input-with-icon');
                 icon.removeClass('fa-check').addClass('fa-exclamation');  
                 parent.removeClass('success-control').addClass('error-control');   

               },
                unhighlight: function (element) { // revert the change done by hightlight

                },
                success: function (label, element) {
                 var icon = $(element).parent('.input-with-icon').children('i');
                 var parent = $(element).parent('.input-with-icon');
                 icon.removeClass("fa-exclamation").addClass('fa-check');
                 parent.removeClass('error-control').addClass('success-control');  
               },
               submitHandler: function(form) {
                $form = $(form);
                $form.ajaxSubmit({ 
                  dataType:  'json',
                  success: processCategoryJSONResponse
                });
                return false;
              }
            });

$('#message_ajax_news_form').hide();
$('.delete-category-form').live('submit', function (e) {
  e.preventDefault();
  $form = $(this);
  var url = $form.attr('action');
  var dataString = $form.serialize();
  $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    async: false,
    dataType: "json",
    success: function(data) {
      $(data).each(function(j,details){
        var status = details.status;
        var message = details.message;
        $('#message_ajax_news_form').show();
        if(status === 0){
          $('#message_ajax_news_form').html('<div class="alert alert-error">'+message+'</div>');
        }
        else if(status == 1){
          $('#message_ajax_news_form').html('<div class="alert alert-success">'+message+'</div>');
          getAllCategories();
        }
                }); //end of $(data) details
            } //end of success
        });//end of $.ajax 
  return false;
});

$('.edit-category-form').live('submit', function (e) {
  e.preventDefault();
  $form = $(this);
  var url = $form.attr('action');
  var dataString = $form.serialize();
  $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    async: false,
    dataType: "json",
    success: function(data) {
      $(data).each(function(j,details){
        var status = details.status;
        var message = details.message;
        $('#message_ajax_news_form').show();
        if(status === 0){
          $('#message_ajax_news_form').html('<div class="alert alert-error">'+message+'</div>');
        }
        else if(status == 1){
          $('#message_ajax_news_form').html('<div class="alert alert-success">'+message+'</div>');
          getAllCategories();
        }
                }); //end of $(data) details
            } //end of success
        });//end of $.ajax 
  return false;
});  


function processCategoryJSONResponse(data) { 
    // 'data' is the json object returned from the server 
    $(data).each(function(j,details){
      var status = details.status;
      var message = details.message;
      $('#message_ajax_register_category').show();
      if(status === 0){
        $('#message_ajax_register_category').html('<div class="alert alert-error">'+message+'</div>');
      }
      else if(status == 1){
        var eventId = details.event_id;
        var eventName = details.event_name;
        $('#message_ajax_register_category').html('<div class="alert alert-success">'+message+'</div>'); 
        $form[0].reset();
        getAllCategories();
      }
    });
}

function processJSONResponse(data) { 
    // 'data' is the json object returned from the server 
    $(data).each(function(j,details){
      var status = details.status;
      var message = details.message;
      $('#message_ajax_register').show();
      if(status === 0){
        $('#message_ajax_register').html('<div class="alert alert-error">'+message+'</div>');
      }
      else if(status == 1){
        var eventId = details.event_id;
        var eventName = details.event_name;
        $('#message_ajax_register').html('<div class="alert alert-success">'+message+'</div>'); 
        $form[0].reset();
      }
    });
}

});
			</script>
            
                
                
    <!-- Register Fest Screen -->
    <div class="grid-100 mobile-grid-100">
	<div class="grid simple horizontal"><div class="grid-title row"><?php echo $breadcrumb; ?><div class="grid-50 mobile-grid-100"><h3 class="semi-bold text-error row">Add Team Member details !!</h3></div><div class="clearfix"></div>
  
  </div><div class="grid-body">
  <div class="numberContainer"><div class="number">Member Details</div></div>
 
  <hr/>
  <div class="row">
	<?php 
	echo "<div id='message_ajax_register'></div>";
	$attributes = array('id'=>'register-form');
    echo form_open('teams/register_event',$attributes);?>
    <input type="hidden" name="fest_id" value="<?php echo $fest_id;?>" id='fest_id'/>
	
    
    <div class="row">
    <div class="form-group">
        <div class="row">

          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('Category<span class="text-error">*</span>','username',$data);?>
            </div>
           <div class="pull-left width-50-form no-margin-top">
              <div class="controls">
          <div class="input-with-icon  right">                                       
            <i class="fa"></i>
            <?php $data = 'id="txtmembercategory" class="form-control"';
            $options = array();
            echo form_dropdown('txtmembercategory', $options,'',$data);
            ?>    

          </div>
              </div>
              <a id="addEditCategories"  class="url">Add/Edit Categories</a>  
            </div>

        </div>
      </div>
    </div>

    <div class="row">
    <div class="form-group">
        <div class="row">
          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('Member Name<span class="text-error">*</span>','username',$data);?>
            </div>
           <div class="pull-left width-50-form no-margin-top">
              <div class="controls">
          <div class="input-with-icon  right">                                       
            <i class="fa"></i>
            <?php $data = array('id'=>'txtmembername','name'=>'txtmembername','class'=>'form-control'); 
              echo form_input($data); ?>                 
          </div>
              </div>
            </div>
        </div>
      </div>
    </div>


    <div class="row">
    <div class="form-group">
        <div class="row">
          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('Email address<span class="text-error">*</span>','username',$data);?>
            </div>
           <div class="pull-left width-50-form no-margin-top">
              <div class="controls">
          <div class="input-with-icon  right">                                       
            <i class="fa"></i>
            <?php $data = array('id'=>'txtmemberemail','name'=>'txtmemberemail','class'=>'form-control'); 
              echo form_input($data); ?>                 
          </div>
              </div>
            </div>
        </div>
      </div>
    </div>


     <div class="row">
    <div class="form-group">
        <div class="row">
          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('Contact Number<span class="text-error">*</span>','username',$data);?>
            </div>
           <div class="pull-left width-50-form no-margin-top">
              <div class="controls">
          <div class="input-with-icon  right">                                       
            <i class="fa"></i>
            <?php $data = array('id'=>'txtmemberphone','name'=>'txtmemberphone','class'=>'form-control'); 
              echo form_input($data); ?>                 
          </div>
              </div>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
    <div class="form-group">
      <div class="row">
          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('Member Image<span class="text-error">*</span>','username',$data);?>
            </div>
            <span class="btn btn-primary fileinput-button">
                    <i class="fa fa-upload"></i>
                    <span>Upload Image</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="fileupload" type="file" name="userfile"  accept="image/*">
              </span>
                (PNG, GIF and JPG allowed. Maximum file size allowed is 1MB.)
            <div class="clearfix"></div>  
        </div>
      </div>
    </div> 
                

    <div class="row">
    <div class="form-group">
        <div class="row">
          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('LinkedIn page URL','username',$data);?>
            </div>
           <div class="pull-left width-50-form no-margin-top">
              <div class="controls">
          <div class="input-with-icon  right">                                       
            <i class="fa"></i>
            <?php $data = array('id'=>'txtlinkedinwebsite','name'=>'txtlinkedinwebsite','class'=>'form-control','placeholder'=>'e.g. https://www.facebook.com/iitb.moodindigo'); 
              echo form_input($data); ?>                 
          </div>
              </div>
            </div>
        </div>
      </div>
    </div>

    
   <div class="row">
    <div class="form-group">
        <div class="row">
          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('Facebook page URL','username',$data);?>
            </div>
           <div class="pull-left width-50-form no-margin-top">
              <div class="controls">
          <div class="input-with-icon  right">                                       
            <i class="fa"></i>
            <?php $data = array('id'=>'txtfacebookwebsite','name'=>'txtfacebookwebsite','class'=>'form-control','placeholder'=>'e.g. https://www.facebook.com/iitb.moodindigo'); 
              echo form_input($data); ?>                 
          </div>
              </div>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
    <div class="form-group">
        <div class="row">
          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('Twitter page URL','username',$data);?>
            </div>
           <div class="pull-left width-50-form no-margin-top">
              <div class="controls">
          <div class="input-with-icon  right">                                       
            <i class="fa"></i>
            <?php $data = array('id'=>'txttwitterwebsite','name'=>'txttwitterwebsite','class'=>'form-control','placeholder'=>'e.g. https://twitter.com/iitb_moodi'); 
              echo form_input($data); ?>                 
          </div>
              </div>
            </div>
        </div>
      </div>
    </div>

    <div class="row">
    <div class="form-group">
        <div class="row">
          <div class="pull-left width-15-form">
              <?php $data = array('class'=>'form-label');
        echo form_label('Google Plus page','username',$data);?>
            </div>
           <div class="pull-left width-50-form no-margin-top">
              <div class="controls">
          <div class="input-with-icon  right">                                       
            <i class="fa"></i>
            <?php $data = array('id'=>'txtgpluswebsite','name'=>'txtgpluswebsite','class'=>'form-control','placeholder'=>'e.g. https://plus.google.com/105531709859584441009/posts'); 
              echo form_input($data); ?>                 
          </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    

    
    <hr class='flag'/>





    <div class="form-actions">  
					<div>
					  <?php $data = array('name'=>'createevent','value'=>'Add','class'=>'btn btn-primary btn-cons');
    					echo form_submit($data);
						echo anchor(base_url().'fest', 'Back', array('class'=>'btn btn-white btn-cons','id'=>'backurl'));	
						?>
    				</div>
	</div>
	<?php	  
	echo form_close();
    ?>
    </div></div></div></div>


<!-- Modal -->
<div class="modal fade" id="addEditCategoriesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
       <h4 class="modal-title" id="modalTitle">Add/Edit Categories</h4>
     </div>
     <div class="modal-body row" id="modalBody">
     


      <div class="grid-50 mobile-grid-100">
        <hr class="flag"/><h2>View All Categories</h2><hr class="flag"/>
        <div id='message_ajax_news_form'></div><div id="categorytable"></div>
      </div>

      <div class="grid-50 mobile-grid-100">
        <hr class="flag"/><h2>Add a New Category</h2><hr class="flag"/>

        <?php 
        echo "<div id='message_ajax_register_category'></div>";
        $attributes = array('id'=>'add-category-form');
        echo form_open('teams/add_new_category',$attributes);?>
        <input type="hidden" name="fest_id" value="<?php echo $fest_id;?>" id='fest_id'/>

        <div class="row">
          <div class="form-group">
            <div class="row">
              <div class="pull-left width-100-form">
                <?php $data = array('class'=>'form-label');
                echo form_label('New Category Name<span class="text-error">*</span>','username',$data);?>
              </div>
              <div class="pull-left width-100-form no-margin-top">
                <div class="controls">
                  <div class="input-with-icon  right">                                       
                    <i class="fa"></i>
                    <?php $data = array('id'=>'txtcategoryname','name'=>'txtcategoryname','class'=>'form-control'); 
                    echo form_input($data); ?>                 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <hr class='flag'/>
        <div class="form-actions">  
          <div>
            <?php $data = array('name'=>'createcategory','value'=>'Add Category','class'=>'btn btn-primary btn-cons');
            echo form_submit($data);
            ?>
          </div>
        </div>
        <?php   
        echo form_close();
        ?>



      </div>
     
     </div>
   </div>
   <!-- /.modal-content -->
 </div>
 <!-- /.modal-dialog -->
</div>
<!-- /.modal -->