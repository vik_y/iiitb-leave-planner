<script>

    $(document).ready(function(){

        var get_articles_category = '<?php echo  base_url()."articles/get_articles_category"?>';
        var category_id = document.URL.split('/').pop();

        var load_list = function(){
           var formData = {'category_id': category_id};
           $.ajax({
                type: 'POST',
                url: get_articles_category,
                data: formData,
                dataType: 'json',
                encode: true
           })
               .done(function(data){
                   console.log(data);
                   if(data.status==0){
                       $('#articles').append(data.message);
                   }
                   else{
                       //console.log(data);
                       var a = data.content;
                       $('#category_name').append(data.category_name);
                       var final = '<div class=col-md-8> <ul class="list-group" >';

                       $.each(a, function(index, item){
                           var url = '<?php echo base_url();?>'+'articles/view/'+item.article_url
                           final += '<a href='+url+'><li class=list-group-item>'+item.article_name+'</li></a>';
                       })
                       final+= '</ul></div>'
                       $('#articles').append(final);
                   }
                   //alert('successfuly');

               })
               .fail(function(data){
                   console.log(data);
                   //alert('failed');
               })
        };
        load_list();

    });

</script>

<div class="grid-100 mobile-grid-100">
    <div class="grid simple horizontal">


        <div class="grid-title row"><?php echo $breadcrumb; ?>
            <div class="grid-50 mobile-grid-100"><h3 id="category_name" class="semi-bold text-error row"></h3></div>
            <div class="clearfix">
                <div class="grid-100" id="articles">


                </div>

            </div>
        </div>




    </div>
</div>
