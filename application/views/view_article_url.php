<!--Redactor Starts Here-->
<!--Start of Redactor Part-->
<link rel="stylesheet" href="<?php echo base_url()?>assets/redactor/redactor.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/redactor/style.css" />
<script src="<?php echo base_url()?>assets/redactor/redactor.min.js"></script>
<!--Redactor Ends Here-->
<script>
    $(document).ready(function(){
        /*Constants Start here */
        var request_url = document.URL.split('/').pop();
        var get_categories_url = '<?php echo base_url().'categories/get_categories'?>';
        var get_article_url =  '<?php echo base_url()."articles/get_article_one"?>';
        var edit_article_url = '<?php echo base_url()."articles/edit_article"?>';
        var delete_article_url = '<?php echo base_url().'articles/delete_article'?>';
        /*Constants End Here*/

        /*
        Function definitions start here
        */
        var get_categories = function(val){
            var return_name;
            $.ajax({
                type: 'GET',
                url: get_categories_url,
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    console.log(data);
                    $.each(data, function(index,item){
                        var name = item.category_name;
                        var id = item.category_id;
                        if(id==val){
                            $('#category_id').append(
                                '<option selected="selected" value=' +id +'>'+name+'</option>'
                            )
                        }
                        else{
                            $('#category_id').append(
                                '<option value=' +id +'>'+name+'</option>'
                            )
                        }
                    })
                })
        };

        /*Load Content function loads everything on the page*/
        var load_content = function(){
            $.ajax({
                type: 'POST',
                url: get_article_url,
                data:  {'article_url':request_url},
                dataType: 'json',
                encode: true
            })
                .done(function(data){
                    console.log(data);
                    $('#articlename').html('<strong>'+data.content[0].article_name+'<small>'+data.category_name+'</small>'+'</strong>');
                    article_content = data.content[0].article_content;
                    //$('#redactor_content').show();
                    $('#article_content').html(data.content[0].article_content);
                    $('#delete_article').val(data.content[0].article_id);
                    $('#article_id').val(data.content[0].article_id);
                    $('#redactor_content').html(data.content[0].article_content); //Adds the text to textarea
                    $('.redactor_').html(data.content[0].article_content); //Adds the text to the text editor
                    $('#article_name').val(data.content[0].article_name);

                    get_categories(data.content[0].article_category_id);

                    $.each(data.sidebar, function(index, item){
                        $('#sidebar').append('<a href='+item.article_url+'><li class=list-group-item>'+item.article_name+'</li></a>');
                    })

                })
                .fail(function(data){
                    console.log(data);
                })
        };
        /*Load Content function ends here*/

        var loader1_on = function(){
            $('#spinner1').show();
        };
        var loader1_off = function(){
            $('#spinner1').hide();
        };

        /*
        Function definition ends here
         */


        $('#edit_article').click(function(){
            event.preventDefault();
            loader1_on();
            var formData = $(this).parent().parent().serialize();
            $.ajax({
                type: 'POST',
                url: edit_article_url,
                data: formData,
                dataType: 'json',
                encode: true
            })
                .done(function(data){
                    console.log(data);
                    load_content();
                    loader1_off();
                    $('#myModal').modal('toggle');
                    //Print success/
                })

                .fail(function(){

                })
        });

        $('#delete_article_button').click(function(){
            if(confirm("Are you sure you want to delete this article?")){
                loader1_on()
                var article_id = $(this).prev().val();
                $.ajax({
                    type: 'POST',
                    url: delete_article_url,
                    data: {article_id: article_id},
                    dataType: 'json',
                    encode: true
                })
                    .done(function(data){
                        loader1_off();
                        console.log(data);
                        if(data.status ==1){
                            alert("Deleted Successfully, you are being redirected to articles page");
                            window.location.replace("<?php echo base_url().'articles'?>")
                        }
                        else{
                            loader1_off();
                            $('#message_ajax_register').html('<div class="alert alert-danger">'+data.message+'</div>');
                            //Put in the message ajax bar.. that error has occured.
                        }
                    })
                    .fail(function(data){
                        loader1_off();
                        console.log(data);
                        $('#message_ajax_register').html('<div class="alert alert-danger">Request Failed</div>');
                        //Put in the message ajax bar.. that error has occured.
                    })
            }



        });
        //get_categories();

        load_content();

        $('#redactor_content').redactor({
            imageUpload: '/eventchum/articles/upload_image'
        });
        //$('#redactor_content').redactor('sync');
        loader1_off();


    });
</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" area-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add a New Article</h4>
            </div>

            <div class="form-group">
                <form id="add_article">
                    <div class="modal-body">

                        <div class="center-text" id="spinner1"><i class="fa fa-spinner fa-5x fa-spin"></i></div>

                        <!--WYSIWYG Editor Starts here -->
                        <div class="control-group">
                            <label class="control-label" for="article_name">Article Name</label>
                            <div class="controls">
                                <input id="article_name" name="article_name" type="text" placeholder="Article Name here" required="">
                                <input id="article_id" name="article_id" type="hidden">

                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="control-group">
                            <label class="control-label" for="selectbasic">Select Basic</label>
                            <div class="controls">
                                <select id="category_id" name="category_id" class="input-xlarge">
                                    <!--Options here will come from jquery-->
                                </select>
                            </div>
                        </div>
                        <br>

                        <textarea id="redactor_content" name="article_content" placeholder="Write and Upload content here">

                        </textarea>

                        <!--WYSIWYG Editor Ends here -->
                    </div>
                    <div class="modal-footer">
                        <button  type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="edit_article" type="button" class="btn btn-primary">Save changes</button>


                    </div>
                    <form>
            </div>

        </div>
    </div>
</div>


<div class="grid-100 mobile-grid-100">
    <div class="grid simple horizontal">

        <div class="grid-title row"><?php echo $breadcrumb; ?>
            <div class="grid-50 mobile-grid-100"><h3 class="semi-bold text-error row">Category_Name</h3></div>
            <div class="clearfix"></div>
        </div>

        <div class="grid-body">
            <div class="container" id="articles">
                <div class="row">
                    <div class="grid-70">

                            <div id="message_ajax_register"></div>
                            <h2 id="articlename"></h2>
                            <button class="btn btn-primary btn-lg btn-danger" id="modal" data-toggle="modal" data-target="#myModal">
                                Edit This Article
                            </button>
                            <form>
                                <input type="hidden" id="delete_article">
                                <button id="delete_article_button" type="button" class="btn btn-primary">Delete Article</button>
                            </form>
                        <hr>
                    </div>
                    <div class="grid-30">
                        <div class="grid simple horizontal">
                            <ul class="list-group" id="sidebar">
                            <h4>Other articles from this category</h4>
                                <hr>
                            </ul>
                        </div>
                    </div>

                    <div class="grid-100">
                        <div class="horizontal">

                            <p class="featured lead" id="article_content"></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

