   </div><!-- /.container -->
    </div>
  <!--#Wrap closed -->
  
  <div class="section white footer">
		<div class="grid-container">
			
      <div class="p-t-30 p-b-50">
				<div class="row">

          <div class="grid-50 mobile-grid-100">
          <img src="<?php echo site_url();?>assets/images/logo.png" alt="" data-src="<?php echo site_url();?>assets/images/logo.png"  width="159" height="42">
          </div>

          <div class=" grid-50 mobile-grid-100">
          <div class="pull-right"><span class="social-networking-label">Follow Us - </span>
            <a target="_blank" href="http://www.facebook.com/eventchum" class="social-networking-buttons social-networking-facebook"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></a>
            <a target="_blank" href="https://www.twitter.com/eventchum" class="social-networking-buttons social-networking-twitter"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span></a>
            <a target="_blank" href="https://www.google.com/+eventchum" class="social-networking-buttons social-networking-google"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-google-plus fa-stack-1x fa-inverse"></i></span></a>
            <a target="_blank" href="https://www.instagram.com/eventchum" class="social-networking-buttons social-networking-instagram"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-instagram fa-stack-1x fa-inverse"></i></span></a>
            <a target="_blank" href="http://www.pinterest.com/eventchum/" class="social-networking-buttons social-networking-pinterest"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-pinterest fa-stack-1x fa-inverse"></i></span></a>
          </div>
          </div>
          
        </div>

			</div>
    


		</div>
</div>

<!-- END CONTAINER -->

<!-- BEGIN CORE TEMPLATE JS -->
<!-- END CORE TEMPLATE JS -->
</div>

<script src="<?php echo base_url();?>assets/js/mix.min.js" type="text/javascript" defer></script>

</body>
</html>