<!--Start of Redactor Part-->
<link rel="stylesheet" href="<?php echo base_url()?>assets/redactor/redactor.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/redactor/style.css" />
<script src="<?php echo base_url()?>assets/redactor/redactor.min.js"></script>
<script>
    $(document).ready(function(){
        var get_articles = '<?php echo base_url().'articles/get_articles'?>';
        var add_article = '<?php echo base_url().'articles/add_article'?>';
        var loader1_on = function(){
            $('#spinner1').show();
        }

        var loader1_off = function(){
            $('#spinner1').hide();
        }
        var loader2_on = function(){
            $('#spinner2').show();
        }

        var loader2_off = function(){
            $('#spinner2').hide();
        }

        var get_categories = function(){
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url().'categories/get_categories'?>',
                dataType: 'json',
                encode: true
            })

                .done(function(data){
                    $.each(data, function(index,item){
                        var name = item.category_name;
                        var id = item.category_id;
                        $('#category_id').append(
                            '<option value=' +id +'>'+name+'</option>'
                        )
                    })
                })

        };


        $('#redactor_content').redactor({
            imageUpload: '/eventchum/articles/upload_image'
        });

        var load_list = function(){
            $.ajax({
                type: 'GET',
                url: get_articles,
                dataType: 'json',
                encode: true
            })
                .done(function(data){
                    $('#article_list').html('');
                    var base = '<?php echo base_url().'articles/view/'?>'
                    console.log(data);
                    var count = 1;
                    $.each(data, function(index, item){
                        var category_id = item.category_id;
                        var category_name = item.category_name;
                        var articles = item.articles;
                        var content = '<div class="grid-50"><div class="grid simple horizontal"><div class="grid-body"><h3>'+category_name+'</h3><a href="articles/categoryview/'+category_id+'"><h5>(View All)</h5></a><ol>'

                        if (articles.length>0){
                            $.each(articles,function(index, object ){
                                console.log(object.article_name)
                                content += '<a href=articles/view/'+object.article_url+'><li>'+object.article_name+'</li></a>'
                                //$('#article_list').append('<li class="list-group-item">' + item.articles.article_id + ' '+item.articles.article_name)
                            })
                            content += '</ol></div></div></div>'
                            $('#article_list').append(content);
                            }
                    })
                })
        }

        load_list();


        get_categories();

        loader1_off();

        var load_articles = function(search, category, page){
            /*
            This function loads articles and different pages also.
            search takes the search string, which you searched for
            search and category are optional parmaeters. Should be used when needed
             */
        };


        /*
        For submission happening but notification not showing, need to fix it later
         */




    });


</script>
<!--End of Redactor Part-->

<div class="grid-100 mobile-grid-100">
    <div class="grid simple horizontal">

        <div class="grid-body">

            <div class="grid-title row"><?php echo $breadcrumb; ?>

                <div class="clearfix"></div>
            </div>
        <!-- Button trigger modal -->


        <!-- Modal -->

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" area-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add a New Article</h4>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="jumbotron">
                <div class="row row-fluid">

                    <div id="message_ajax_register"> </div>
                    <h1>Eventchum Help Articles</h1>

                </div>
            </div>
        </div>



            <br>
            <div class="grid-100">
                    <ul id="article_list" class="list-group">

                    </ul>

            </div>
        </div>
    </div>
</div>
