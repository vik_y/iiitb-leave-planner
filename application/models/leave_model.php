<?php class Leave_model extends CI_Model{

    public function __construct() {
        parent::__construct();
        $this->db->query("SET time_zone='+5:30'");
    }

    public function add_employee($data){
        $this->db->insert('employee', $data);
    }

    public function add_leave($data){
        $this->db->insert('leave', $data);
        return array('message'=>'Leave added successfully', 'status'=>1);
    }

    public function get_leave_count($data){
        $r = $this->db->get_where('employee', $data);
        return unserialize($r->row(1)->emp_count);
    }
    public function get_history($data){
        $result = $this->db->get_where('leave', $data);
        return $result->result();
    }

    public function cancel_leave_user($data){
        $query = $this->db->get_where('leave', $data);
        if ($query->num_rows > 0)
        {
            if($query->row(1)->leave_flag == 0){
                $this->db->delete('leave', $data);
                return array('message'=>"Leave Cancelled Successfully", "status"=>1);
            }
            else if($query->row(1)->leave_flag == 1){
                return array('message'=>"Your leave application has already been accepted by the Admin and can't be cancelled now, please contact admin for more information ", "status"=>0);
            }
            else if($query->row(1)->leave_flag == -1){
                return array('message'=>"Your leave has already been rejected by the admin", "status"=>0);
            }
        }

        return array('message'=>"leave record not found", "status"=>0);
    }

    public function get_applications(){
        $result = $this->db->get_where('leave', array('leave_flag'=>0));
        if($result->num_rows()==0){
            return 0;
        }
        else return $result->result();
    }

    public function reject_leave_admin($data){
        $query = $this->db->get_where('leave', $data);
        if ($query->num_rows > 0)
        {
            $this->db->update('leave', array('leave_flag'=> -1), $data);
            return array('message'=>"Leave Rejected Successfully", "status"=>1);

        }

        return array('message'=>"leave record not found", "status"=>0);
    }

    public function accept_leave_admin($data){
        $query = $this->db->get_where('leave', $data);
        if ($query->num_rows > 0)
        {
            $this->db->update('leave', array('leave_flag'=> 1), $data);
            //As the leave is accepted here, you need to change the leave count in the database
            //Write a function to load the leave count from the database
            return array('message'=>"Leave Accepted Successfully", "status"=>1);

        }

        return array('message'=>"leave record not found", "status"=>0);
    }
}