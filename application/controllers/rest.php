<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * NOTE : there has been no form validation - add form validation after testing is completeds
 */
class Rest extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->breadcrumb->add('Home', base_url());
    }



    public function index()
    {
        echo 'Working ';
    }

    public function development(){
        $this->load->view('test');
    }

    public function add_employee(){
        if($this->input->post()){
            $emp_type = $this->input->post('type');
            if($emp_type==1){
                $emp_count = array(
                    "sick"=>"10",
                    "paid"=>"20",
                    "medical"=>"30"
                );

            }
            $data = array(
                'emp_name' => $this->input->post('name'),
                'emp_type' => $this->input->post('type'),
                'emp_email' => $this->input->post('email'),
                'emp_password' => md5($this->input->post('password')),
                'emp_count' => serialize($emp_count)
            );
            $this->load->model('leave_model');
            $this->leave_model->add_employee($data);
            echo "emp added";
        }
        $this->load->view('leave_emp_add');
    }

    public function edit_employee(){

    }

    public function delete_employee(){

    }

    public function get_leave_count(){
        header('Access-Control-Allow-Origin: *');
        if($this->input->get()){
            $args = array("emp_id"=>$this->input->get('leave_parent_id'));
            $this->load->model('leave_model');
            $data = $this->leave_model->get_leave_count($args);
        }
        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $json;
    }

    public function apply_for_leave(){
        header('Access-Control-Allow-Origin: *');

        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');

            $data = array(
                //NOTE: Leave parent ID fid not added yet
                "leave_type"=> $this->input->post('leave_type'),
                "leave_parent_id"=>$this->input->post('leave_parent_id'),
                "leave_from"=>$this->input->post('from'),
                "leave_to"=>$this->input->post('to'),
            );

            $this->form_validation->set_rules('leave_type', 'Leave Type', 'trim|required|xss_clean|min_length[1]');
            $this->form_validation->set_rules('from', 'From', 'trim|required|xss_clean|min_length[1]');
            $this->form_validation->set_rules('to', 'To', 'trim|required|xss_clean|min_length[1]');

            if($this->form_validation->run() == false){
                $message = validation_errors();
                $data = array('message' => $message,'status'=>0);
            }
            else{
                $this->load->model('leave_model');
                //Array will be returned from the below function which will have status and message
                $response = $this->leave_model->add_leave($data);
                $data = array('message' => $response['message'],'status'=>$response['status']);
            }

            //file_put_contents('newfile.dat',$data);

        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Leave details are required";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $json;
    }

    public function get_leave_history(){
        header('Access-Control-Allow-Origin: *');
        // $this->output->enable_profiler(TRUE);
        //Complete asap
        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');
            $data = array(
                //NOTE: Leave parent ID field not added yet
                "leave_parent_id"=> $this->input->post('leave_parent_id'),
            );
            $this->form_validation->set_rules('leave_parent_id', 'Leave Type', 'trim|required|xss_clean|min_length[1]');

            if($this->form_validation->run() == false){
                $message = validation_errors();
                $data = array('message' => $message,'status'=>0);
            }
            else{
                $this->load->model('leave_model');
                //Array will be returned from the below function which will have status and message
                $response = $this->leave_model->get_history($data);
                $data = $response;
                //file_put_contents('newfile.dat',$data);
            }
        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Leave details required";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $json;
    }

    public function get_applications(){
        //NO authentication till now
        //Add admin authentication and session verification later on

            $this->load->model('leave_model');

            //Array will be returned from the below function which will have status and message
            $response = $this->leave_model->get_applications();
            if($response==0) $data = array('message' => "No pending leave applications",'status'=>0);
            else $data = array('message' => $response,'status'=>1);
            //file_put_contents('newfile.dat',$data);


        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $json;
    }

    public function cancel_leave_application(){
        header('Access-Control-Allow-Origin: *');
        //Get unique user id and session id of the user, see if that session id is assigned - make a table
        //If assigned then cancel the leave application

        //Poor code, don't use it for production
        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');
            $data = array(
                //NOTE: Leave parent ID field not added yet
                "leave_id"=> $this->input->post('leave_id'),
            );
            $this->load->model('leave_model');
            //Array will be returned from the below function which will have status and message
            $response = $this->leave_model->cancel_leave_user($data);
            $data = $response;
            //file_put_contents('newfile.dat',$data);
        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Leave ID required";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $json;

        }

        //$session_id =  $this->input->post('session_id'); //Take session id for authentication



    public function accept_leave_admin(){
        header('Access-Control-Allow-Origin: *');
        //Raw function
        //Not for production
        //No admin authentication
        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');
            $data = array(
                //NOTE: Leave parent ID field not added yet
                "leave_id"=> $this->input->post('leave_id'),
            );
            $this->load->model('leave_model');
            //Array will be returned from the below function which will have status and message
            $response = $this->leave_model->accept_leave_admin($data);
            $data = $response;
            //file_put_contents('newfile.dat',$data);
        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Leave ID required";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $json;

    }


    public function reject_leave_admin(){
        header('Access-Control-Allow-Origin: *');
        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');
            $data = array(
                //NOTE: Leave parent ID field not added yet
                "leave_id"=> $this->input->post('leave_id'),
            );
            $this->load->model('leave_model');
            //Array will be returned from the below function which will have status and message
            $response = $this->leave_model->reject_leave_admin($data);
            $data = $response;
            //file_put_contents('newfile.dat',$data);
        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Leave ID required";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $json;
    }

    private function notify_email(){

    }


    /*----------------------------Rest API Functions end here ----------------- */



    public function edit_article(){
        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');

            $this->form_validation->set_rules('article_name', 'Article Name', 'trim|required|xss_clean|min_length[2]');
            $this->form_validation->set_rules('category_id', 'Category Id', 'trim|required|xss_clean|min_length[1]');
            $this->form_validation->set_rules('article_content', 'Content', 'trim|required|xss_clean|min_length[10]');
            $this->form_validation->set_rules('article_id', 'Article ID', 'trim|required|xss_clean|min_length[1]');


            if($this->form_validation->run() == false){
                $message = validation_errors();
                $data = array('message' => $message,'status'=>0);
            }
            else{
                $articleName = $this->input->post('article_name');
                $article_category_id = $this->input->post('category_id');
                $articleContent = $this->input->post('article_content');
                $articleId = $this->input->post('article_id');
                //$articleURL =  $this->create_uri($articleName);

                $data = array(
                    "article_name"=>$articleName,
                    "article_id"=>$articleId,
                    "article_category_id"=>$article_category_id,
                    "article_content"=>$articleContent,
                );
                $this->load->model('article_model');
                //Array will be returned from the below function which will have status and message
                $response = $this->article_model->edit_article($data);
                $data = array('message' => $response['message'],'status'=>$response['status']);
                //file_put_contents('newfile.dat',$data);
            }
        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Article details are required";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $data;
    }

    public function get_article_one(){
        //$this->output->enable_profiler(TRUE);

        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');

            $this->form_validation->set_rules('article_url', 'Article URL', 'trim|required|xss_clean|min_length[2]');


            if($this->form_validation->run() == false){

                $message = validation_errors();
                $data = array('message' => $message,'status'=>0);
            }
            else{
                $articleUrl = $this->input->post('article_url');
                $this->load->model('article_model');

                //Array will be returned from the below function which will have status and message
                $response = $this->article_model->get_article_one($articleUrl);
                $data = $response;
                //file_put_contents('newfile.dat',$data);
            }
        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Invalid";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $data;
    }

    public function delete_article(){
        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');

            $this->form_validation->set_rules('article_id', 'Article ID', 'trim|required|xss_clean|min_length[1]');


            if($this->form_validation->run() == false){

                $message = validation_errors();
                $data = array('message' => $message,'status'=>0);
            }
            else{
                $articleId = $this->input->post('article_id');
                $this->load->model('article_model');
                $data['article_id'] = $articleId;
                //Array will be returned from the below function which will have status and message
                $response = $this->article_model->delete_article($data);
                $data = $response;
                //file_put_contents('newfile.dat',$data);
            }
        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Invalid";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $data;
    }

    public function view(){

        $breadcrumb = $this->breadcrumb->output();
        $this->breadcrumb->add('Articles', base_url().'articles');
        $this->breadcrumb->add('View', current_url());
        $breadcrumb = $this->breadcrumb->output();
        $data = array('breadcrumb'=> $breadcrumb);

        $this->load->view('view_help_header');
        $this->load->view('view_article_url',$data);
        $this->load->view('view_help_footer');

    }

    public function upload_image(){
        $filename = md5($_FILES['file']['name']);
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '100000';
        $config['max_width']  = '10240';
        $config['max_height']  = '7680';
        $config['file_name'] = $filename;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            $data = array('error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('filelink' => base_url().'uploads/'.$filename.'.png');
        }
        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $data;

    }

    public function get_articles_category(){
        //should return articles of a category
        if($this->input->post())
        {
            //start validation
            $this->load->library( array('form_validation'));
            $this->load->helper('form');

            $this->form_validation->set_rules('category_id', 'Category ID required', 'trim|required|xss_clean|min_length[1]');


            if($this->form_validation->run() == false){
                $message = validation_errors();
                $data = array('message' => $message,'status'=>0);
            }
            else{
                $categoryId = $this->input->post('category_id');
                $this->load->model('article_model');
                $inp['article_category_id'] = $categoryId;
                //Array will be returned from the below function which will have status and message
                $response = $this->article_model->get_articles_category($inp);
                $data = $response;
                //file_put_contents('newfile.dat',$data);
            }
        }
        else{
            //If opened this method without post method, this will be displayed.
            $message = "Invalid Method";
            $data = array('message' => $message,'status'=>0);
        }

        $this->output->set_content_type('application/json');
        $json = $this->output->set_output(json_encode($data));
        //return to the view once reach here
        return $data;
    }

    public function categoryview(){
        $breadcrumb = $this->breadcrumb->output();
        $this->breadcrumb->add('Articles', base_url().'articles');
        $this->breadcrumb->add('Category_Name_to bechanged', current_url());
        $breadcrumb = $this->breadcrumb->output();
        $data = array('breadcrumb'=> $breadcrumb);

        $this->load->view('view_help_header');
        $this->load->view('view_category_one', $data);
        $this->load->view('view_help_footer');
    }

    public function search_articles(){

        if($this->input->get()){
            $search_string = $this->input->get('string');
            $search_string = $this->security->xss_clean($search_string);
            $this->load->model('article_model');
            $data= $this->article_model->search_article($search_string);

        }
        else{
            $data = array('status'=>0, 'message'=>'Invalid Request');
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
        /*
         * Function for searching an article, returns all the articles which match.
         * Search the entire article
         */

    }


    public function search(){

        $breadcrumb = $this->breadcrumb->output();
        $this->breadcrumb->add('Articles', base_url().'articles');
        $this->breadcrumb->add('Search Result', current_url());
        $breadcrumb = $this->breadcrumb->output();
        $data['breadcrumb']= $breadcrumb;

        if($this->input->get() and strlen($this->input->get('string'))>=3){
            $search_string = $this->input->get('string');
            $search_string = $this->security->xss_clean($search_string);
            $data['search_string'] = $search_string;
            $this->load->view('view_help_header');
            $this->load->view('view_articles_search', $data);
            $this->load->view('view_help_footer');
        }
        else{
            echo "Error"; //Dispaly error page
        }
    }

    public function share_count(){
        require_once(APPPATH.'libraries/sharecount'.EXT);
        $obj = new shareCount('http://www.mashable.com');
        echo $obj->get_tweets().'<br>'; //to get tweets
        echo $obj->get_fb().'<br>'; //to get facebook total count (likes+shares+comments)
        echo $obj->get_linkedin().'<br>'; //to get linkedin shares
        echo $obj->get_plusones().'<br>'; //to get google plusones
        echo $obj->get_pinterest().'<br>'; //to get pinterest pins
    }
    /* Functions */

    /* Slug creation starts */

    /**
     * Create a uri string
     *
     * This wraps into the _check_uri method to take a character
     * string and convert into ascii characters.
     *
     * @param   mixed (string or array)
     * @param   int
     * @uses    Slug::_check_uri()
     * @uses    Slug::create_slug()
     * @return  string
     */
    public function create_uri($data = '', $id = '')
    {
        if (empty($data))
        {
            return FALSE;
        }

        if (is_array($data))
        {
            if ( ! empty($data[$this->field]))
            {
                return $this->_check_uri($this->create_slug($data[$this->field]), $id);
            }
            elseif ( ! empty($data[$this->title]))
            {
                return $this->_check_uri($this->create_slug($data[$this->title]), $id);
            }
        }
        elseif (is_string($data))
        {
            return $this->_check_uri($this->create_slug($data), $id);
        }

        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Create Slug
     *
     * Returns a string with all spaces converted to underscores (by default), accented
     * characters converted to non-accented characters, and non word characters removed.
     *
     * @param   string $string the string you want to slug
     * @param   string $replacement will replace keys in map
     * @return  string
     */
    public function create_slug($string)
    {
        $this->load->helper(array('url', 'text', 'string'));
        $string = strtolower(url_title(convert_accented_characters($string), '_'));
        return reduce_multiples($string, $this->_get_replacement(), TRUE);
    }

    // ------------------------------------------------------------------------

    /**
     * Check URI
     *
     * Checks other items for the same uri and if something else has it
     * change the name to "name-1".
     *
     * @param   string $uri
     * @param   int $id
     * @param   int $count
     * @return  string
     */
    private function _check_uri($uri, $id = FALSE, $count = 0)
    {
        $new_uri = ($count > 0) ? $uri.$this->_get_replacement().$count : $uri;
        $data['article_url'] = $new_uri;
        $this->load->model('article_model');
        $response = $this->article_model->check_slug_exists($data);
        if($response == "1"){
            return $this->_check_uri($uri, $id, ++$count);
        }
        else
        {
            return $new_uri;
        }

    }

    // ------------------------------------------------------------------------

    /**
     * Get the replacement type
     *
     * Either a dash or underscore generated off the term.
     *
     * @return string
     */


    private function _get_replacement()
    {
        return '_';
    }

    /* slug creation ends */

    /* Breadcrumbs */

}